# RStudio Selenized
This is my implementation of the palette [Selenized](https://github.com/jan-warchol/selenized/tree/master) for RStudio. To produce the themes I used [tmTheme editor](https://tmtheme-editor.glitch.me/).

## Installation
Themes can be installed via “Tools” - “Global Options” - “Appearance” - “Add Theme”. See [this page](https://support.posit.co/hc/en-us/articles/115011846747-Using-Themes-in-the-RStudio-IDE) for documentation on RStudio themes.

## Preview
### Selenized
![](./img/light.png)

### Selenized Dark
![](./img/dark.png)
